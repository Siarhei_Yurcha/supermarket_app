//
//  ContentView.swift
//  Supermarket
//
//  Created by Siarhei Yurcha on 21.04.22.
//

import SwiftUI

struct ListView: View {
    @ObservedObject var viewModel: ListViewModel = ListViewModel(observeCartInteractor: WSObserveCartInteractor())
    
    @ViewBuilder
    var body: some View {
        List {
            ForEach(viewModel.listItems) { item in
                ListItemView(item: item)
            }
        }
        .navigationTitle("Cart")
        .toolbar {
            Button(viewModel.connectionButtonTitle) {
                viewModel.handleConnectionButtonPress()
            }
        }
        .onAppear {
            viewModel.handleOnAppear()
        }
        .onDisappear {
            viewModel.handleOnDisappear()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
