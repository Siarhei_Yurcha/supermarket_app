//
//  ListViewModel.swift
//  Supermarket
//
//  Created by Siarhei Yurcha on 21.04.22.
//

import Foundation
import SwiftUI
import Combine
import Starscream

final class ListViewModel: ObservableObject {
    
    // MARK: Constants
    
    private enum Constants {
        static let start = "start"
        static let stop = "stop"
    }

    // MARK: - Properties
    
    @Published var listItems: [CartItem] = []
    @Published var connectionButtonTitle: String = ""
    
    private var cart = Cart()
    private var observeCartInteractor: ObserveCartInteractor
    private var observingCancellables: [AnyCancellable] = []
    
    // MARK: - Init
    
    init(observeCartInteractor: ObserveCartInteractor) {
        self.observeCartInteractor = observeCartInteractor
        
        setupBindings()
    }
    
    // MARK: Private API
    
    private func setupBindings() {
        cart.$items.assign(to: &$listItems)
    }
    
    func startObserving() {
        connectionButtonTitle = Constants.stop
        observeCartInteractor.run()
            .catch { _ in Empty().eraseToAnyPublisher() }
            .sink { [weak cart] item in cart?.add(item: item) }
            .store(in: &observingCancellables)

    }
    
    func stopObserving() {
        connectionButtonTitle = Constants.start
        observingCancellables = []
        cart.sort()
    }
    
    // MARK: - Public API

    func handleConnectionButtonPress() {
        if observingCancellables.isEmpty {
            startObserving()
        } else {
            stopObserving()
        }
    }
    
    func handleOnAppear() {
        startObserving()
    }
    
    func handleOnDisappear() {
        stopObserving()
    }
}
