//
//  ListItemView.swift
//  Supermarket
//
//  Created by Siarhei Yurcha on 21.04.22.
//

import Foundation
import SwiftUI

struct ListItemView: View {
    
    let item: CartItem
    
    var body: some View {
        HStack(alignment: .center, spacing: 10) {
            Color(uiColor: UIColor(hex: item.bagColor) ?? .white)
                .frame(width: 40, height: 40)
                .cornerRadius(20)
                .overlay(RoundedRectangle(cornerRadius: 20)
                        .stroke(Color.gray, lineWidth: 1))
            
            VStack(alignment: .leading) {
                Text(item.name).padding()
                Divider()
                Text(item.weight).padding()
            }
            .overlay(RoundedRectangle(cornerRadius: 5)
                    .stroke(Color.gray, lineWidth: 1))
        }
    }
}
