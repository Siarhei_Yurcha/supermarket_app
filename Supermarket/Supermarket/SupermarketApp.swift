//
//  SupermarketApp.swift
//  Supermarket
//
//  Created by Siarhei Yurcha on 21.04.22.
//

import SwiftUI

@main
struct SupermarketApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ListView()
            }
        }
    }
}
