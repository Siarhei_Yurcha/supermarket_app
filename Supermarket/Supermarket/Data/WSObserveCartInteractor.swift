//
//  WSObserveCartInteractor.swift
//  Supermarket
//
//  Created by Siarhei Yurcha on 21.04.22.
//

import Foundation
import Combine
import Starscream

struct WSObserveCartInteractor: ObserveCartInteractor {
    
    // MARK: - Error
    
    enum Error: Swift.Error {
        case unknown
    }
    
    // MARK: - Constants
    
    private enum Constants {
        static let connectionURL = URL(string:"ws://superdo-groceries.herokuapp.com/receive")!
    }
    
    // MARK: - Public API
    
    func run() -> AnyPublisher<CartItem, Swift.Error> {
        var socket: WebSocket?
        let subject = PassthroughSubject<CartItem, Swift.Error>()
        var countOfSubscribers = 0

        return subject
            .receive(on: DispatchQueue.main)
            .handleEvents(receiveSubscription: { _ in
                countOfSubscribers += 1
                
                guard socket == nil else { return }
                
                let request = URLRequest(url: Constants.connectionURL)
                socket = WebSocket(request: request)
                socket?.connect()
                socket?.onEvent = { event in
                    switch event {
                    case .text(let string):
                        guard let data = string.data(using: .utf8) else { return }
                        do {
                            subject.send(try JSONDecoder().decode(CartItem.self, from: data))
                        } catch {
                            // ignore broken item
                        }
                    case .ping, .pong, .connected, .viabilityChanged, .reconnectSuggested, .binary:
                        break
                    case .cancelled:
                        subject.send(completion: .finished)
                    case .error, .disconnected:
                        subject.send(completion: .failure(Error.unknown))
                    }
                }
            }, receiveCancel: {
                countOfSubscribers -= 1
                if countOfSubscribers == 0 {
                    socket?.disconnect()
                }
            })
            .eraseToAnyPublisher()
    }
}
