//
//  File.swift
//  Supermarket
//
//  Created by Siarhei Yurcha on 21.04.22.
//

import Foundation

class Cart {
    @Published var items: [CartItem] = []
    
    func add(item: CartItem) {
        items.insert(item, at: 0)
    }
    
    func sort() {
        items = items.sorted { $0.bagColor > $1.bagColor }
    }
}

struct CartItem: Codable, Identifiable {
    
    private enum CodingKeys: String, CodingKey {
            case name, bagColor, weight
    }
    
    let id: UUID = UUID()
    
    let name: String
    let bagColor: String
    let weight: String
    
}
