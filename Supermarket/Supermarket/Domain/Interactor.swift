//
//  UseCases.swift
//  Supermarket
//
//  Created by Siarhei Yurcha on 21.04.22.
//

import Foundation
import Combine

protocol ObserveCartInteractor {
    func run() -> AnyPublisher<CartItem, Error>
}
